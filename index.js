var app = require("express")();
var db = require("./db.js");
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.get('/', function (req, res){
	res.send("Hello World!")
})

app.get('/testdb', function(req, res){
	db.query("SELECT * FROM test", function(err, result){
		if(!err){
			res.json(result.rows)
		}else{
			res.send("Error a la DB");
			console.log(err);
		}
	})
})

app.get('/apps', function(req, res){
        db.query("SELECT * FROM app", function(err, result){
                if(!err){
                        res.json(result.rows)
                }else{
                        res.send("Error a la DB");
                        console.log(err);
                }
        })
})

app.get('/:id', function(req, res){
        db.query("SELECT * FROM record WHERE app_code = '" + req.params.id + "'", function(err, result){
                if(!err){
                        res.json(result.rows)
                }else{
                        res.send("Error 404");
                        console.log(err);
                }
        })
})

app.post('/:app_id', function(req, res){
        db.query("INSERT INTO record(app_code, player, score) VALUES ('"+ req.params.app_id +"','"+ req.body.id_player +"',"+req.body.num_score+")", function(err, result){
                if(!err){
                        res.json(result.rows)
                }else{
                        res.send("Error 404");
                        console.log(err);
                }
        })
})


app.listen(process.env.PORT || 3000, function(){
	console.log("Example")
})
