var pg = require('pg');
exports.query = function(query, params, callback) {
  var connectionString = "postgres://ulzapeztzfqqib:83ff796f743f3efe8f1431985c9616ee52dcfccafa24505803659947ba21e67c@ec2-50-17-236-15.compute-1.amazonaws.com:5432/d5a37qjg3vt9hu";
  if (typeof params == "function") {
    callback = params;
    params = [];
  }
  pg.connect(connectionString  + '?ssl=true', function(err, client, done) {
    if (!err) {
      console.info(query);
      client.query(query, params, function(err, result) {
        done();
        callback(err, result);
      })
    } else {
      callback(err);
      return console.error(err);
    }
  });
};
